package s2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Produs {

    private JPanel dsd;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JButton button1;
    public Produs(){
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String string = textField1.getText();
                int x = Integer.parseInt(string);
                String string2 = textField2.getText();
                int y = Integer.parseInt(string2);
                int p;
                p = x * y;
                textField3.setText("The produce is: " + p);
            }
        });
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("Produs");
        frame.setContentPane(new Produs().dsd);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setSize(250,250);
    }
}
